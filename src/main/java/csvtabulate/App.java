package csvtabulate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App 
{
	public List<String> csvImportList(){
        return Arrays.asList(
            "Name;Strasse;Ort;Alter",
            "Peter Pan;Am Hang 5;12345 Einsam;42",
            "Maria Schmitz;Kölner Straße 45;50123 Köln;43",
            "Paul Meier;Münchener Weg 1;87654 München;65"
        );
	}
	
    public static void main( final String[] args) {
		final App app = new App();

		final List<String> table = app.tabulate(app.csvImportList());

		for (final String row : table) {
			System.out.println(row);
		}
	}

	public List<String> tabulate(final List<String> csvInput) {

		final List<String> table = new ArrayList<>();

		// find optimal column space
		final int[] columnSpace = findColumnSpace(csvInput);

		int index = 0;
		String row = "";
		for (int i = 0; i <= csvInput.size(); ++i) {
			if (i == 1) {
				row = tableSeparatorLineBuilder(columnSpace);
			} else {
				final String[] columns = convertRowToColumns(csvInput, index);
				row = tableRowBuilder(columns, columnSpace);
				++index;
			}
			table.add(row);
		}

		return table;
	}

	public int[] findColumnSpace(final List<String> csvInput) {

		final String[] firstRow = convertRowToColumns(csvInput, 0);
		final int numberOfColumns = firstRow.length;

		final int[] columnsSpace = new int[numberOfColumns];

		for (int i = 0; i < csvInput.size(); ++i) {
			final String[] currentRow = convertRowToColumns(csvInput, i);

			int currentColumn = 0;
			for (final String column : currentRow) {
				if (columnsSpace[currentColumn] < column.length()) {
					columnsSpace[currentColumn] = column.length();
				}
				++currentColumn;
			}
		}
		return columnsSpace;
	}

	public String[] convertRowToColumns(final List<String> csvInput, final int index) {
		return csvInput.get(index).split(";");
	}

	public String tableRowBuilder(final String[] columns, final int[] columnSpace) {

		final StringBuilder rowBuilder = new StringBuilder();

		for (int i = 0; i < columns.length; ++i) {
			final String column = columns[i];
			final int whitespaces = columnSpace[i] - column.length();

			rowBuilder.append(column);

			for (int j = 0; j < whitespaces; ++j) {
				rowBuilder.append(' ');
			}

			rowBuilder.append('|');
		}

		return rowBuilder.toString();
	}

	public String tableSeparatorLineBuilder(final int[] columnSpace) {

		final StringBuilder separatorLineBuilder = new StringBuilder();

		for (final int element : columnSpace) {

			for (int j = 0; j < element; ++j) {
				separatorLineBuilder.append('-');
			}
			separatorLineBuilder.append('+');
		}

		return separatorLineBuilder.toString();
	}

}
