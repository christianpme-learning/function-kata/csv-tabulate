package csvtabulate;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class AppTest 
{
    public List<String> csvExampleList(){
        return Arrays.asList(
            "Name;Strasse;Ort;Alter",
            "Peter Pan;Am Hang 5;12345 Einsam;42",
            "Maria Schmitz;Kölner Straße 45;50123 Köln;43",
            "Paul Meier;Münchener Weg 1;87654 München;65"
        );
    }

    public String[] headerColumnsExampleArray(){
        return new String[]{
            "Name",
            "Strasse",
            "Ort",
            "Alter"
        };
    }

    public List<String> tableExampleList(){
        return Arrays.asList(
            "Name         |Strasse         |Ort          |Alter|",
            "-------------+----------------+-------------+-----+",
            "Peter Pan    |Am Hang 5       |12345 Einsam |42   |",
            "Maria Schmitz|Kölner Straße 45|50123 Köln   |43   |",
            "Paul Meier   |Münchener Weg 1 |87654 München|65   |"
        );
    }

    @Test
    public void tabulateTest(){
        
        final List<String> csvInput = csvExampleList();

        final App app = new App();
        final List<String> actual = app.tabulate(csvInput);

        final List<String> expected = tableExampleList();

        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void findColumnSpaceTest() {

        final List<String> csvInput = csvExampleList();

        final App app = new App();
        final int[] actual = app.findColumnSpace(csvInput);

        // Maria Schmitz;Kölner Straße 45;87654 München;Alter
        final int[] expected = new int[] { 13, 16, 13, 5 };

        assertArrayEquals(expected, actual);
    }

    @Test
    public void convertRowToColumnsTest() {

        final List<String> csvInput = csvExampleList();

        // header example row
        final int index = 0;

        final App app = new App();
        final String[] actual = app.convertRowToColumns(csvInput, index);

        final String[] expected = headerColumnsExampleArray();

        assertArrayEquals(expected, actual);
    }

    @Test
    public void tableRowBuilderTest() {

        final List<String> csvInput = csvExampleList();
        final String[] columns = headerColumnsExampleArray();

        final App app = new App();

        final int[] columnSpace = app.findColumnSpace(csvInput);

        final String actual = app.tableRowBuilder(columns, columnSpace);

        final String expected = tableExampleList().get(0);

        assertEquals(expected, actual);
    }

    @Test
    public void tableSeparatorLineBuilderTest() {

        final List<String> csvInput = csvExampleList();

        final App app = new App();

        final int[] columnSpace = app.findColumnSpace(csvInput);

        final String actual = app.tableSeparatorLineBuilder(columnSpace);

        final String expected = tableExampleList().get(1);

        assertEquals(expected, actual);
    }
    
}
